var TwitterUtils = Class.create();
TwitterUtils.prototype = {
    initialize: function() {},

    getRecentTweets: function(users) {
        try {
            if (typeof(users) === 'string') {
                var rm = new sn_ws.RESTMessageV2();
                rm.setHttpMethod('get');
                rm.setRequestHeader('Authorization', 'Bearer ' + gs.getProperty('x_657773_twitter.bearer_token')); // You need a Bearer Token from a developer Twitter account

                var query = 'from%3A' + users.replaceAll(',', '%20OR%20from%3A');
                var endpoint = 'https://api.twitter.com/2/tweets/search/recent' + '?query=' + query;
                endpoint += '&expansions=author_id,attachments.media_keys';
                endpoint += '&tweet.fields=created_at';
                endpoint += '&user.fields=name';
                endpoint += '&media.fields=url';

                var lastTweet = new GlideRecord('x_657773_twitter_tweets');
                lastTweet.orderByDesc('created_at');
                lastTweet.query();
                if (lastTweet.next()) {
                    endpoint += '&since_id=' + lastTweet.getValue('tweet_id');
                }

                rm.setEndpoint(endpoint);
                var response = rm.execute();
                return response.getBody();
            } else {
                throw 'Invalid user list format. Please insert a comma separated list of users as a string.';
            }
        } catch (err) {
            gs.error(err);
        }
    },

    parseTweets: function(responseBody) {
        try {
            var parsed = JSON.parse(responseBody);
            var tweets = parsed.data;
            var authors = {},
                attachments = {};

            if (!gs.nil(tweets)) {
                var user_data = gs.nil(parsed.includes.users) ? [] : parsed.includes.users;
                var media_data = gs.nil(parsed.includes.media) ? [] : parsed.includes.media;

                var user = {}; // { user id : username } object for users
                for (var i = 0; i < user_data.length; i++) {
                    user = user_data[i];
                    authors[user.id] = user.name;
                }

                var media = {}; // { media key : url } object for attached media
                for (i = 0; i < media_data.length; i++) {
                    media = media_data[i];
                    attachments[media.media_key] = media.url;
                }
            }

            return {
                tweets: tweets,
                authors: authors,
                attachments: attachments
            };


        } catch (err) {
            gs.info(err);
        }
    },

    saveTweets: function(parsedTweets) {
        try {
            var tweets = parsedTweets.tweets,
                authors = parsedTweets.authors,
                attachments = parsedTweets.attachments;

            var tweet = {},
                tweetGR = {},
                time = '',
                tweetSysId = '',
                mediaKeys = [],
                download = {},
                url = '';

            for (i = 0; i < tweets.length; i++) {
                tweet = tweets[i];
                tweetGR = new GlideRecord('x_657773_twitter_tweets');

                if (!tweetGR.get('tweet_id', tweet.id)) { // Tweet id is checked to avoid having multiple records of the same tweet
                    tweetGR.initialize();
                    tweetGR.setValue('tweet_id', tweet.id);
                    tweetGR.setValue('text', tweet.text);
                    time = tweet.created_at;
                    time = time.replace('T', ' ').replace(/\..*/, '');
                    tweetGR.setValue('created_at', time);
                    tweetGR.setValue('author', authors[tweet.author_id]);
                    tweetSysId = tweetGR.insert();
                    if (!gs.nil(tweet.attachments)) {
                        mediaKeys = tweet.attachments.media_keys;
                        if (!gs.nil(mediaKeys)) {
                            for (var l = 0; l < mediaKeys.length; l++) {
                                download = new sn_ws.RESTMessageV2();
                                download.setHttpMethod('get');
                                url = attachments[mediaKeys[l]];
                                download.setEndpoint(url);
                                download.saveResponseBodyAsAttachment('x_657773_twitter_tweets', tweetSysId, url.match(/[^/]*$/)[0]); // Extracts the name from the ending part of the url
                                download.execute();
                            }
                        }
                    }

                }
            }
        } catch (err) {
            gs.info(err);
        }
    },

    type: 'TwitterUtils'
};
