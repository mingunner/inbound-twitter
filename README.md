# Inbound Twitter

Simple inbound Twitter integration for ServiceNow. The twitterUtils.js file is the script for a Script Include (class record) and the main function is to retrieve the most recent tweets from a list of users and save them on a custom table.

For Twitter API to work it is needed a Bearer Token, obtainable from a developer twitter account. It can be saved as an encrypted system property.

Script Include, custom table and token are all within the same application scope, *x_657773_twitter* in my case.

### Example usage

```javascript
var tu = new x_657773_twitter.TwitterUtils;
var users = 'Beta80Group,80Tow';
var rt = tu.getRecentTweets(users);
var pt = tu.parseTweets(rt);
tu.saveTweets(pt);
```
